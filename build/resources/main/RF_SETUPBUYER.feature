Feature: RF_SETUP_BUYER

  Background:
    Given Run the base url

  Scenario Outline: User is trying to do all the buyers operations

    Given GET the list of buyers
    And create a POST for buyer already exists
    And DELETE the buyer
    And DELETE non existing buyer
    And GET the newly created buyer
    And UPDATE the buyer
    #Then check the buyer list "<code>" code "<description>" desc "<targetEntityType>" type "<targetEntityuuid>" uuid

   Examples:
      |code|description|targetEntityType|targetEntityuuid|postcode|
      |NPD_DOM_COMP|CONFIRM2|COMPANY|8d8327d6-6e8c-2022-e053-0100007f41c0|201|


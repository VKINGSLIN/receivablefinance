Feature: RF_SETUP_SUPPLIER

  Background:
    Given Run the base url

  Scenario Outline: User is trying to do all the buyers operations

    Given GET the list of suppliers
    And create a POST for supplier already exists
    And DELETE the supplier
    And DELETE non existing supplier
    And GET the newly created supplier
    And UPDATE the supplier

    Examples:
      |code|description|targetEntityType|targetEntityuuid|postcode|
      |NPD_DOM_COMP|CONFIRM2|COMPANY|8d8327d6-6e8c-2022-e053-0100007f41c0|201|




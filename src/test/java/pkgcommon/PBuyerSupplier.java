package pkgcommon;

public class PBuyerSupplier {


    public String code;
    public String description;
    public String targetEntityType;
    public String targetEntityUuid;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTargetEntityType() {
        return targetEntityType;
    }

    public void setTargetEntityType(String targetEntityType) {
        this.targetEntityType = targetEntityType;
    }

    public String getTargetEntityUuid() {
        return targetEntityUuid;
    }

    public void setTargetEntityUuid(String targetEntityUuid) {
        this.targetEntityUuid = targetEntityUuid;
    }




}

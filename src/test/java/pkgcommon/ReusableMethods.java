package pkgcommon;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ReusableMethods {

    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Defines Base url
     *Input:NA
     *Output:NA
     */

    public void restassuredBASEURL() {

        RestAssured.baseURI = "http://127.0.0.1:8090";

    }

    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Defines Static Headers
     *Input:NA
     *Output:request object
     */

    public static RequestSpecification staticHeaders() {

        RequestSpecification req = RestAssured.given().
                header("Kyriba-User-Code", "KITS@ADMIN")
                .header("Content-Type", "application/json")
                .header("Accept", "*/*")
                .header("Connection", "keep-alive")
                .header("Kyriba-Api-Gateway", "value");
        return req;

    }

    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Common method for POST operation
     *Input:jsoninput, path
     *Output:returns response
     */

    public static Response makePOST(String createjson, String path) throws IOException {

        RequestSpecification request = staticHeaders();
        request.body(createjson);
        Response response = request.post(path);
        return response;

    }

    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Common method for PUT operation
     *Input:jsoninput, path, uuid
     *Output:returns response
     */

    public static Response makePUT(String createjson, String path,String uuid) throws IOException {

        RequestSpecification request = staticHeaders();
        request.body(createjson);
        Response response = request.put(path + uuid);
        return response;

    }

    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Common method for Assertion
     *Input:Actualstatuscode, ExpectedStatuscode
     *Output:statuscode
     */

    public static int assertStatusCode(int statusCode, int expectedstatusCode){


        Assert.assertEquals(statusCode, expectedstatusCode);
        return statusCode;
    }

    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Create new buyer/supplier
     *Input:code,desc,Entitytype,Entityuuid,path
     *Output:returns jsoninput
     */
    public static String setPOSTvalues(String code, String desc, String Entitytype, String Entityuuid, String path) throws IOException {

        PBuyerSupplier objbuyer = new PBuyerSupplier();
        objbuyer.setCode(code);
        objbuyer.setDescription(desc);
        objbuyer.setTargetEntityType(Entitytype);
        objbuyer.setTargetEntityUuid(Entityuuid);

        ObjectMapper objectMapper = new ObjectMapper();
        String createjson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(objbuyer);
        PBuyerSupplier objbuyer2 = objectMapper.readValue(createjson, PBuyerSupplier.class);
        System.out.println("code : " + objbuyer2.getCode());
        System.out.println("desc : " + objbuyer2.getDescription());
        System.out.println("targetentity : " + objbuyer2.getTargetEntityType());
        System.out.println("targetentityuuid: " + objbuyer2.getTargetEntityUuid());

        return createjson;
    }

    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Common method for GET operation
     *Input:path
     *Output:returns response
     */

    public static Response makeGET(String path) throws IOException {
        RequestSpecification request = staticHeaders();
        Response response = request.get(path);
        return response;

    }

    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Common method for DELETE operation
     *Input:path, uuid
     *Output:returns response
     */

    public static Response makeDELETE(String path, String uuid) throws IOException {
        RequestSpecification request = staticHeaders();
        Response response = request.delete(path + uuid);
        String responsecontent = response.getBody().asString();
        return response;

    }


}

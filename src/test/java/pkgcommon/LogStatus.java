package pkgcommon;

public class LogStatus extends Report {

    public static void delete(int statuscode){

        if(statuscode==200) {
            test.log(com.relevantcodes.extentreports.LogStatus.PASS,"Delete call is successfull"+" "+"Statuscode:"+statuscode);
        }
        else{

            test.log(com.relevantcodes.extentreports.LogStatus.FAIL,"Delete call is not successfull"+" "+"Statuscode:"+statuscode);
        }
    }



    public static void post(String statuscode){

        int number = Integer.parseInt(statuscode);
        System.out.println(number);

        if(number==201) {
            test.log(com.relevantcodes.extentreports.LogStatus.PASS,"Post call is successfull"+" "+"Statuscode:"+statuscode);
        }
        else{

            test.log(com.relevantcodes.extentreports.LogStatus.FAIL,"Post call is not successfull"+" "+"Statuscode:"+statuscode);
        }
    }

    public static void get(int statuscode){

        if(statuscode==200) {
            test.log(com.relevantcodes.extentreports.LogStatus.PASS,"Get call is successfull"+" "+"Statuscode:"+statuscode);
        }
        else{

            test.log(com.relevantcodes.extentreports.LogStatus.FAIL,"Get call is not successfull"+" "+"Statuscode:"+statuscode);
        }
    }

    public static void update(int statuscode){

        if(statuscode==200) {
            test.log(com.relevantcodes.extentreports.LogStatus.PASS,"Update call is successfull"+" "+"Statuscode:"+statuscode);
        }
        else{

            test.log(com.relevantcodes.extentreports.LogStatus.FAIL,"Update call is not successfull"+" "+"Statuscode:"+statuscode);
        }
    }


}

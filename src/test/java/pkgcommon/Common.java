package pkgcommon;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Common {

        public static ExtentTest test;
        public static ExtentReports report;
        public static String Reportappend = null;


        public static void startTest() {

            if (Reportappend == null) {
                report = new ExtentReports(System.getProperty("user.dir") + "/target/" + "ExtentReportResults.html");
                test = report.startTest("Buyer_Supplier_Execution_Report");
                Reportappend = "Reportcreated";
            } else {
                System.out.println("report already exists");
            }

        }


        public static void deletelog(int statuscode, String name) {

            if (statuscode == 200) {
                test.log(LogStatus.PASS, "Delete call is successfull for " + name + " " + "Statuscode:" + statuscode);
            } else {

                test.log(LogStatus.FAIL, "Delete call is not successfull for " + name + " " + "Statuscode:" + statuscode);
            }

        }

    public static void negative(int statuscode, String name) {

        if (statuscode == 404) {
            test.log(LogStatus.PASS, "Delete call is successfull for " + name + " " + "Statuscode:" + statuscode);
        } else {

            test.log(LogStatus.FAIL, "Delete call is not successfull for " + name + " " + "Statuscode:" + statuscode);
        }

    }


        public static void postlog(String statuscode, String name) {

            int number = Integer.parseInt(statuscode);
            System.out.println(number);

            if (number == 201) {
                test.log(LogStatus.PASS, "Post call is successfull for " + name + " " + "Statuscode:" + statuscode);
            } else {

                test.log(LogStatus.FAIL, "Post call is not successfull for " + name + " " + "Statuscode:" + statuscode);
            }

        }

        public static void getlog(int statuscode, String name) {

            if (statuscode == 200) {
                test.log(LogStatus.PASS, "Get call is successfull for " + name + " " + "Statuscode:" + statuscode);
            } else {

                test.log(LogStatus.FAIL, "Get call is not successfull for " + name + " " + "Statuscode:" + statuscode);
            }

        }

        public static void updatelog(int statuscode, String name) {

            if (statuscode == 200) {
                test.log(LogStatus.PASS, "Update call is successfull for " + name + " " + "Statuscode:" + statuscode);
            } else {

                test.log(LogStatus.FAIL, "Update call is not successfull for " + name + " " + "Statuscode:" + statuscode);
            }

        }



        public static void endTest() {

            report.endTest(test);
            report.flush();

        }

        public static String getdateandtime(){

            String[] datearr = new String[2];
            Date date = Calendar.getInstance().getTime();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String strDate = dateFormat.format(date);
            datearr = strDate.split(" ");
            String calender = datearr[0];
            String newcalen = calender.replace(" ", "");
            String calender1 = datearr[1];
            String newcalen1 = calender1.replace(":", "");

            return newcalen1;
        }

}

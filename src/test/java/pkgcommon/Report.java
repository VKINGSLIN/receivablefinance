package pkgcommon;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class Report {
        static ExtentTest test;
        static ExtentReports report;
        @BeforeClass
        public static void startTest()
        {

            report = new ExtentReports(System.getProperty("user.dir") + "/target/FinalReport"+ "ExtentReportResults.html");
            test = report.startTest("ExtentDemo");
        }

        @AfterClass
        public static void endTest()
        {
            report.endTest(test);
            report.flush();
        }
    }






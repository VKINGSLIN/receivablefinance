package pkgstepdefenition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pkgcommon.Common;
import pkgcommon.ReusableMethods;
import pkgmodules.BuyerAndSupplier;

import java.io.IOException;

public class REST_STEPDEFENITION_BUYER extends ReusableMethods {

    public static String path = "/api/v1/buyers/";
    public static String name = "Buyer";
    public static String code = "NPD_DOM_COMP";
    public static String desc = "CONFIRM2";
    public static String type = "COMPANY";
    public static String uuid = "8d8327d6-6e8c-2022-e053-0100007f41c0";
    public static String[] buyerarray = new String[10];
    //int[] myIntArray = new int[3];
    public static String[] responsearray;

    @BeforeMethod
    @Given("Run the base url")
    public void runTheBaseUrl() {

    restassuredBASEURL();

    }

    @Test
    @Given("GET the list of buyers")
    public static void getTheListOfBuyers() throws IOException {

        System.out.println("iam in fisrt method");
        buyerarray = BuyerAndSupplier.checkList(path);
        if (buyerarray[0] == null) {
            System.out.println("buyre value is" + buyerarray[0]);
            responsearray = BuyerAndSupplier.POSTBuyer(code, desc, type, uuid, path);
        }
        buyerarray = BuyerAndSupplier.checkList(path);
        BuyerAndSupplier.POSTexistingBuyer(buyerarray[0], buyerarray[1], buyerarray[2], buyerarray[3], path, name);

        Common.startTest();
    }

    //@Test(priority = 1)
    @Test (dependsOnMethods={"getTheListOfBuyers"})
    @And("DELETE the buyer")
    public static void deleteTheBuyer() throws IOException {
        System.out.println("array" + buyerarray);
        System.out.println("value of buyer array is " + buyerarray[0]);
        int outputcode = BuyerAndSupplier.delete(buyerarray, path);
        Common.deletelog(outputcode, name);

        int outputcode1 = BuyerAndSupplier.deletenonexist(buyerarray, path);
        Common.negative(outputcode1, name);

        //2
        int Scode = BuyerAndSupplier.getnonexist(buyerarray, path);
        Common.negative(Scode, name);

    }

    @Test (dependsOnMethods={"deleteTheBuyer"})
    @And("GET the newly created buyer")
    public void getTheNewlyCreatedBuyer() throws IOException {

        responsearray = BuyerAndSupplier.POSTBuyer(code, desc, type, uuid, path);
        buyerarray=BuyerAndSupplier.get(responsearray,code, desc, type, uuid,path);
        int Scode = Integer.parseInt(buyerarray[5]);
        System.out.println(Scode);
        Common.getlog(Scode, name);


    }

    @Test (dependsOnMethods={"getTheNewlyCreatedBuyer"})
    @And("UPDATE the buyer")
    public void updateTheBuyer() throws IOException {

        int Statuscode=BuyerAndSupplier.update(buyerarray,path);
        Common.updatelog(Statuscode,name);
        Common.endTest();
    }


    }



package pkgmodules;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.Assert;
import pkgcommon.Common;
import pkgcommon.ReusableMethods;

import java.io.IOException;

public class BuyerAndSupplier extends ReusableMethods {

    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Create new buyer/supplier
     *Input:code,desc,Entitytype,Entityuuid,path
     *Output:returns responsearray
     */

    public static String[] POSTBuyer(String code, String desc, String Entitytype, String Entityuuid, String path) throws IOException {

        String[] buyer_Array = new String[3];
        String createjson=setPOSTvalues(code,desc,Entitytype,Entityuuid,path);
        Response response = makePOST(createjson,path);
        int statuscode = response.getStatusCode();
        String temp = Integer.toString(statuscode);
        int runtimeCode=assertStatusCode(statuscode,201);
        if(runtimeCode==201){
            JsonPath jsonevaluator = response.jsonPath();
            buyer_Array[0] = jsonevaluator.getString("uuid");
            System.out.println("the created uuid is" + buyer_Array[0]);
            buyer_Array[1] = jsonevaluator.getString("code");
            System.out.println("the created code" + buyer_Array[1]);
            buyer_Array[2] = temp;
            //response validation
            Assert.assertEquals(buyer_Array[1] , code);
            if(buyer_Array[0]!=null){
            }
            else
            {
                Assert.fail("uuid is empty");
            }
        }
        else
        {
            Assert.fail("POST method is not successfull");
        }

        return buyer_Array;

    }


    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Create existing buyer/supplier
     *Input:code,desc,Entitytype,Entityuuid,path
     *Output:NA
     */

    public static void POSTexistingBuyer(String code, String desc, String Entitytype, String Entityuuid, String path,String name) throws IOException {
        Boolean val;
        String createjson=setPOSTvalues(code,desc,Entitytype,Entityuuid,path);
        Response response = makePOST(createjson,path);
        String bodyAsString = response.asString();
        int statuscode = response.getStatusCode();
        String temp = Integer.toString(statuscode);
        int runtimeCode=assertStatusCode(statuscode,400);
        if(runtimeCode==400){
            if(name=="Supplier"){

               val=bodyAsString.contains("Supplier for this entity already exists");

            }
            else{

            val=bodyAsString.contains("Buyer for this entity already exists");
            }
                if(val==true){

                    System.out.println("Response validation is successfull");
                }
                else{

                    Assert.fail("Response doesn't contain expected message");
                }
            }
        else
        {
            Assert.fail("POST method is not successfull");
        }

    }


    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Checks the list of existing buyer/supplier
     *Input:path
     *Output:returns a single buyer/supplier
     */

    public static String[] checkList(String param) throws IOException {
        String[] buyer_Array = new String[5];
        Response response = makeGET(param);
        int statuscode = response.getStatusCode();
        int runtimeCode=assertStatusCode(statuscode,200);
        if(runtimeCode==200) {
            JsonPath jsonevaluator = response.jsonPath();
            buyer_Array[0] = jsonevaluator.getString("results[0].code");
            if (buyer_Array[0] == null) {
            } else {
                buyer_Array[0] = jsonevaluator.getString("results[0].code");
                buyer_Array[1] = jsonevaluator.getString("results[0].description");
                buyer_Array[2] = jsonevaluator.getString("results[0].targetEntityType");
                buyer_Array[3] = jsonevaluator.getString("results[0].targetEntityUuid");
                buyer_Array[4] = jsonevaluator.getString("results[0].uuid");
            }
        }
        return buyer_Array;
    }

    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Checks the list of existing buyer/supplier
     *Input:path
     *Output:returns a single buyer/supplier
     */

    public static String[] getList(String path) throws IOException {

        String[] buyer_Array = new String[5];
        Response response = makeGET(path);
        int statuscode = response.getStatusCode();
        int runtimeCode=assertStatusCode(statuscode,200);
        if(runtimeCode==200) {
            JsonPath jsonevaluator = response.jsonPath();
            buyer_Array[0] = jsonevaluator.getString("results[0].code");
            buyer_Array[1] = jsonevaluator.getString("results[0].description");
            buyer_Array[2] = jsonevaluator.getString("results[0].targetEntityType");
            buyer_Array[3] = jsonevaluator.getString("results[0].targetEntityUuid");
            buyer_Array[4] = jsonevaluator.getString("results[0].uuid");
        }
        return buyer_Array;
    }

    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Delete a buyer/supplier
     *Input:value[4] contains uuid to be deleted
     *Output:statuscode
     */

    public static int delete(String[] value, String path) throws IOException {
        String uuid = value[4];
        Response response = makeDELETE(path,uuid);
        int statuscode = response.getStatusCode();
        int runtimeCode=assertStatusCode(statuscode,200);
        if(runtimeCode==200) {
            JsonPath jsonevaluator = response.jsonPath();
            String[] buyer_Array = new String[1];
            buyer_Array[0] = jsonevaluator.getString("uuid");
            System.out.println("the created uuid is" + buyer_Array[0]);
            //response validation
            Assert.assertEquals(buyer_Array[0], value[4]);
        }
        return statuscode;

    }

    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Delete nonexisting buyer/supplier
     *Input:value[4] contains uuid to be deleted
     *Output:statuscode
     */

    public static int deletenonexist(String[] value, String path) throws IOException {
        String uuid = value[4];
        Response response = makeDELETE(path,uuid);
        String bodyAsString = response.asString();
        int statuscode = response.getStatusCode();
        int runtimeCode=assertStatusCode(statuscode,404);
        String Expectedvalue="Receivable entity with "+value[4]+" is not found!";
        if(runtimeCode==404) {
            Boolean val=bodyAsString.contains(Expectedvalue);
            if(val==true){

                System.out.println("Response validation is successfull");
            }
            else{

                Assert.fail("Response doesn't contain expected message");
            }

        }
        return statuscode;

    }

    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Get a buyer/supplier
     *Input:responsearray,code,desc,type,targetuuid,path
     *Output:buyer_Array
     */

    public static String[] get(String[] responsearray, String code, String desc, String type, String targetuuid, String path) throws IOException {
        String[] buyer_Array = new String[6];
        String tempuuid = responsearray[0];
        System.out.println("the temp uuid is"+tempuuid);
        Response response = makeGET(path+tempuuid);
        int statuscode = response.getStatusCode();
        String temp = Integer.toString(statuscode);
        int runtimeCode=assertStatusCode(statuscode,200);
        if(runtimeCode==200) {
            JsonPath jsonevaluator = response.jsonPath();

            buyer_Array[0] = jsonevaluator.getString("code");
            Assert.assertEquals(buyer_Array[0] , code);
            buyer_Array[1] = jsonevaluator.getString("description");
            Assert.assertEquals(buyer_Array[1] , desc);
            buyer_Array[2] = jsonevaluator.getString("targetEntityType");
            Assert.assertEquals(buyer_Array[2] , type);
            buyer_Array[3] = jsonevaluator.getString("targetEntityUuid");
            Assert.assertEquals(buyer_Array[3] , targetuuid);
            buyer_Array[4] = jsonevaluator.getString("uuid");
            Assert.assertEquals(buyer_Array[4] , tempuuid);
            buyer_Array[5] =temp;
        }

        return buyer_Array;

    }

    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:Get a nonexisting buyer/supplier
     *Input:value with uuid, path
     *Output:statuscode
     */

    public static int getnonexist(String[] value, String path) throws IOException {
        String uuid = value[4];
        Response response = makeGET(path+uuid);
        int statuscode = response.getStatusCode();
        String bodyAsString = response.asString();
        int runtimeCode=assertStatusCode(statuscode,404);
        String Expectedvalue="Receivable entity with "+uuid+" is not found!";
        if(runtimeCode==404) {
            Boolean val=bodyAsString.contains(Expectedvalue);
            if(val==true){

                System.out.println("Response validation is successfull");
            }
            else{

                Assert.fail("Response doesn't contain expected message");
            }

        }
        return statuscode;

    }



    /*
     *Author:Kingslin Victoria
     *Date:16-07-2021
     *Description:update a buyer/supplier
     *Input:oldarr with the following values code,desc,Entitytype,Entityuuid and path
     *newarr with uuid
     *Output:NA
     */

    public static int update(String[] newarr, String path) throws IOException {

        String newcalen1= Common.getdateandtime();
        String desc="Time" + newcalen1 + "updatednow";
        String createjson=setPOSTvalues(newarr[0],desc,newarr[2],newarr[3],path);
        Response response = makePUT(createjson,path,newarr[4]);
        int statuscode = response.getStatusCode();
        String temp = Integer.toString(statuscode);
        int runtimeCode=assertStatusCode(statuscode,200);
        if(runtimeCode==200) {
            String[] buyer_Array = new String[2];
            JsonPath jsonevaluator = response.jsonPath();
            buyer_Array[0] = jsonevaluator.getString("uuid");
            System.out.println("the created uuid is" + buyer_Array[0]);
            buyer_Array[1] = jsonevaluator.getString("code");
            System.out.println("the created code" + buyer_Array[1]);
            //response validation
            Assert.assertEquals(buyer_Array[0], newarr[4]);
            if (buyer_Array[0] != null) {
            } else {
                Assert.fail("uuid is empty");
            }
        }
        return statuscode;
    }


}

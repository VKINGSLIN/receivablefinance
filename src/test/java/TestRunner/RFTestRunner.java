package TestRunner;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.text.SimpleDateFormat;
import java.util.Date;
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "/root/IdeaProjects/RF_API/src/main/resources/RF_SETUPBUYER.feature" ,
        glue = {"stepDefinition/REST_STEPDEFENITION_BUYER"} ,
        dryRun = true,
        plugin = { "pretty","html:target/cucumber-reports" },
        publish = true,
        monochrome = true )
class TestRunner {

    public  String ResultFileName;

    @BeforeClass
    public static void setUp()
    {
        String timestamp =new SimpleDateFormat("yyyyMMDDHHmmss").format(new Date());
        String ResultFileFolder="TestReport_"+timestamp;
        String ResultFileName="target/FinalReport/"+ResultFileFolder+"report.html";

    }

    @AfterClass
    public static void writeExtentReport() {



    }
}
/*
@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/main/resources/RF_SETUPBUYER.feature"},
        glue = {"pkgstepdefenition/REST_STEPDEFENITION_BUYER.class"},

        //tags = "FQT-01",
        //tags= {"@FQT-66,@FQT-239,@FQT-240,@FQT-241,@FQT-242"},
        dryRun = true,
        monochrome = true
)
public class RFTestRunner {


}
*/
